resource "aws_instance" "dev-machine-1" {
	ami = "ami-08d2d8b00f270d03b"
//	ami = "ami-02676464a065c9c05"
	instance_type = "t2.medium"
//	associate_public_ip_address = true	
//	provisioner "remote-exec" {
//		inline = [
//     		"sudo amazon-linux-extras install ansible2 -y",
//      		"sudo yum install git -y",
//      		"git clone https://github.com/devops-school/ansible-hello-world-role /tmp/ans_ws",
//      		"ansible-playbook /tmp/ans_ws/site.yaml"
//    	]
//	}
	connection {
		agent = false
		type = "ssh"
		user = "centos"
		private_key = file("geocitizen")
		host = self.public_ip
	}

	provisioner "remote-exec" {
		inline = [
			"sudo yum -y install epel-release yum-utils",
			"sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo",
//			"sudo yum -y install ansible git python3 docker-ce docker-ce-cli containerd.io policycoreutils-python libselinux-python3",
            "sudo yum -y install curl ansible git python3 policycoreutils-python libselinux-python3",
			"curl -fsSL https://get.docker.com -o get-docker.sh",
			"sudo sh get-docker.sh",
			"sudo systemctl enable docker --now",
			"sudo pip3 install docker docker-compose",
			"git clone -b 15.0.1 https://github.com/ansible/awx.git",
			"cd awx/installer",
			"echo admin_password=dimasBP79 >> inventory",
			"sudo ansible-playbook -i inventory install.yml",
			"sudo systemctl restart docker",
			"sudo ansible-playbook -i inventory install.yml"
		]
	}
	vpc_security_group_ids = [aws_security_group.ingress-ssh.id]
//	subnet_id = "${aws_subnet.subnet-internal.id}"
	key_name = aws_key_pair.creds.key_name
}

resource "aws_instance" "dev-machine-n" {
	count = 2
    ami = "ami-08d2d8b00f270d03b"
    instance_type = "t2.medium"
    connection {
		agent = false
        type = "ssh"
        user = "centos"
        private_key = file("geocitizen")
        host = self.public_ip
    }
	vpc_security_group_ids = [aws_security_group.ingress-ssh.id]
    key_name = aws_key_pair.creds.key_name
}

resource "aws_key_pair" "creds" {
	key_name = "dev_key"
	public_key = file("geocitizen.pub")
}

//resource "aws_vpc" "dev-network" {
//  cidr_block = "10.0.0.0/16"
//  enable_dns_hostnames = true
//  enable_dns_support = true
//  tags = {
//    Name = "dev-network"
//  }
//}

resource "aws_eip" "public_address" {
	instance = aws_instance.dev-machine-1.id
	vpc = true
}


//resource "aws_subnet" "subnet-internal" {
//  cidr_block = "${cidrsubnet(aws_vpc.dev-network.cidr_block, 3, 1)}"
//  vpc_id = "${aws_vpc.dev-network.id}"
//  availability_zone = "us-west-1a"
//}

resource "aws_security_group" "ingress-ssh" {
	name = "insecure-group"
	description = "Allow HTTP(S) and SSH traffic"

	ingress {
		description = "HTTP"
		cidr_blocks = ["0.0.0.0/0"]
        	from_port = 80
        	to_port = 80
        	protocol = "tcp"
	}
	ingress {
		description = "SSH"
		cidr_blocks = ["0.0.0.0/0"]
		from_port = 22
		to_port = 22
		protocol = "tcp"
	}
    ingress {
        description = "Grafana"
        cidr_blocks = ["0.0.0.0/0"]
        from_port = 3000
        to_port = 3000
        protocol = "tcp"
    }
	ingress {
        description = "Jenkins"
        cidr_blocks = ["0.0.0.0/0"]
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
    }
	ingress {
        description = "Nexus3"
        cidr_blocks = ["0.0.0.0/0"]
        from_port = 2003
        to_port = 2003
        protocol = "tcp"
    }
    ingress {
        description = "Stats"
        cidr_blocks = ["0.0.0.0/0"]
        from_port = 8081
        to_port = 8081
        protocol = "tcp"
    }
	egress {
		from_port = 0
		to_port = 0
		protocol = "-1"
		cidr_blocks = ["0.0.0.0/0"]
	}
	tags = {
		Name = "terraform"
	}
}
