variable "node_count" {
	default = "3"
}
variable "region" {
	default = "europe-west3"
}
variable "zone" {
	default = "europe-west3-a"
}
variable "project" {
	default = "geocitizencv"
}
variable "author" {
	default = "new_bember"
}
variable "public_key_path" {
    description = "Path to file containing public key"
    default     = "geocitizen.pub"
}
variable "public_key_path2" {
    description = "Path to file containing public key"
    default     = "geocitizen2.pub"
}
variable "public_key_path3" {
    description = "Path to file containing public key"
    default     = "geocitizen3.pub"
}
variable "address" {
    description = "The private IP address to assign to the instance. If empty, the address will be automatically assigned."
    default     = ""
}

variable "nat_ip" {
    description = "The IP address that will be 1:1 mapped to the instance's network ip. If not given, one will be generated."
    default     = ""
}
